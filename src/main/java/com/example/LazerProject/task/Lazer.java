package com.example.LazerProject.task;



import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class Lazer implements Gun {

    @Value("${lazer.shoot.sound:Pew pew pew }")
    private String shootingSound;

    public void shoot (){
        log.info(shootingSound);
    }


}
