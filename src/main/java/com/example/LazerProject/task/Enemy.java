package com.example.LazerProject.task;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Enemy {
    private Gun weapon;

    public Enemy(@Qualifier ("lazer") Gun weapon) {
        this.weapon = weapon;
    }



    private void shoot (){
        weapon.shoot();
    }
}
