package com.example.LazerProject.task;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ShootingRange implements CommandLineRunner {

    private final Person person;


    @Override
    public void run(String... args) throws Exception {
        person.shoot();
    }
}
