package com.example.LazerProject.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PizzaGun implements Gun{
    @Override
    public void shoot() {
        log.info("Smurk smurk smurk");
    }
}
