package com.example.LazerProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LazerProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(LazerProjectApplication.class, args);
	}

}
