package com.example.LazerProject.jpa;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Person {
    @Id
    private Long id;
    String name;
    int age;
}
